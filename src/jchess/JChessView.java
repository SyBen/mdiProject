/*
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package jchess;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTabbedPane;
import javax.swing.Timer;

import jchess.core.Chessboard;
import jchess.core.Colors;
import jchess.core.Game;
import jchess.core.pieces.Piece;
import jchess.core.pieces.implementation.Bishop;
import jchess.core.pieces.implementation.King;
import jchess.core.pieces.implementation.Knight;
import jchess.core.pieces.implementation.Pawn;
import jchess.core.pieces.implementation.Queen;
import jchess.core.pieces.implementation.Rook;
import jchess.display.windows.JChessAboutBox;
import jchess.display.windows.PawnPromotionWindow;
import jchess.display.windows.ThemeChooseWindow;
import jchess.utils.GUI;
import jchess.utils.Settings;

import org.apache.log4j.Logger;
import org.jdesktop.application.Action;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.TaskMonitor;

/**
 * The application's main frame.
 */
public class JChessView extends FrameView implements ActionListener,
		ComponentListener {
	private static final Logger LOG = Logger.getLogger(JChessView.class);

	protected static GUI gui = null;

	/**
	 * @return the gui
	 */
	public static GUI getGui() {
		return gui;
	}

	public Game addNewTab(String title) {
		Game newGUI = new Game();
		this.gamesPane.addTab(title, newGUI);
		return newGUI;
	}

	public void actionPerformed(ActionEvent event) {
		Object target = event.getSource();
		int newGameTime = -1;
		if (target == newGameItem) {
			this.setNewGameFrame(new NewGameWindow());
			JChessApp.getApplication().show(this.getNewGameFrame());
		}
		/*
		 * else if (target == saveGameItem) //saveGame { if
		 * (this.gamesPane.getTabCount() == 0) {
		 * JOptionPane.showMessageDialog(null,
		 * Settings.lang("save_not_called_for_tab")); return; } while (true)
		 * //until { JFileChooser fc = new JFileChooser(); int retVal =
		 * fc.showSaveDialog(this.gamesPane); if (retVal ==
		 * JFileChooser.APPROVE_OPTION) { File selFile = fc.getSelectedFile();
		 * Game tempGUI = (Game)
		 * this.gamesPane.getComponentAt(this.gamesPane.getSelectedIndex()); if
		 * (!selFile.exists()) { try { selFile.createNewFile(); } catch
		 * (IOException exc) { LOG.error("error creating file: " + exc); } }
		 * else if (selFile.exists()) { int opt =
		 * JOptionPane.showConfirmDialog(tempGUI, Settings.lang("file_exists"),
		 * Settings.lang("file_exists"), JOptionPane.YES_NO_OPTION); if (opt ==
		 * JOptionPane.NO_OPTION)//if user choose to now overwrite { continue;
		 * // go back to file choose } } if (selFile.canWrite()) {
		 * tempGUI.saveGame(selFile); }
		 * LOG.debug(fc.getSelectedFile().isFile()); break; } else if (retVal ==
		 * JFileChooser.CANCEL_OPTION) { break; }
		 * ///JChessView.gui.game.saveGame(fc.); } }
		 */
		/*
		 * else if (target == loadGameItem)//loadGame { JFileChooser fc = new
		 * JFileChooser(); int retVal = fc.showOpenDialog(this.gamesPane); if
		 * (retVal == JFileChooser.APPROVE_OPTION) { File file =
		 * fc.getSelectedFile(); if (file.exists() && file.canRead()) {
		 * Game.loadGame(file); } } }
		 */
		else if (target == this.themeSettingsMenu) {
			try {
				ThemeChooseWindow choose = new ThemeChooseWindow(
						this.getFrame());
				JChessApp.getApplication().show(choose);
			} catch (Exception exc) {
				JOptionPane.showMessageDialog(JChessApp.getApplication()
						.getMainFrame(), exc.getMessage());
				LOG.error("Something wrong creating window - perhaps themeList is null: "
						+ exc);
			}
		} else if (target == this.setTime1) {
			newGameTime = 1;
		} else if (target == this.setTime3) {
			newGameTime = 3;
		} else if (target == this.setTime5) {
			newGameTime = 5;
		} else if (target == this.setTime8) {
			newGameTime = 8;
		} else if (target == this.setTime10) {
			newGameTime = 10;
		} else if (target == this.setTime15) {
			newGameTime = 15;
		} else if (target == this.setTime20) {
			newGameTime = 20;
		} else if (target == this.setTime25) {
			newGameTime = 25;
		} else if (target == this.setTime30) {
			newGameTime = 30;
		} else if (target == this.setTime60) {
			newGameTime = 60;
		} else if (target == this.setTime120) {
			newGameTime = 120;
		}

		if (newGameTime != -1) {
			newGameTime *= 60;
			Game game = JChessApp.getJavaChessView().getActiveTabGame();
			game.getSettings().setTimeForGame(newGameTime);
			game.getGameClock().setTimes(newGameTime, newGameTime);
			game.repaint();
		}
	}

	// /--endOf- don't delete, becouse they're interfaces for MouseEvent

	public JChessView(SingleFrameApplication app) {
		super(app);
		initComponents();
		// status bar initialization - message timeout, idle icon and busy
		// animation, etc
		ResourceMap resourceMap = getResourceMap();
		int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
		messageTimer = new Timer(messageTimeout, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				statusMessageLabel.setText("");
			}
		});
		messageTimer.setRepeats(false);
		int busyAnimationRate = resourceMap
				.getInteger("StatusBar.busyAnimationRate");
		for (int i = 0; i < busyIcons.length; i++) {
			busyIcons[i] = resourceMap
					.getIcon("StatusBar.busyIcons[" + i + "]");
		}
		busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
				statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
			}
		});
		idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
		statusAnimationLabel.setIcon(idleIcon);
		progressBar.setVisible(false);

		// connecting action tasks to status bar via TaskMonitor
		TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
		taskMonitor
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						String propertyName = evt.getPropertyName();
						switch (propertyName) {
						case "started":
							if (!busyIconTimer.isRunning()) {
								statusAnimationLabel.setIcon(busyIcons[0]);
								busyIconIndex = 0;
								busyIconTimer.start();
							}
							progressBar.setVisible(true);
							progressBar.setIndeterminate(true);
							break;
						case "done":
							busyIconTimer.stop();
							statusAnimationLabel.setIcon(idleIcon);
							progressBar.setVisible(false);
							progressBar.setValue(0);
							break;
						case "message":
							String text = (String) (evt.getNewValue());
							statusMessageLabel.setText((text == null) ? ""
									: text);
							messageTimer.restart();
							break;
						case "progress":
							int value = (Integer) (evt.getNewValue());
							progressBar.setVisible(true);
							progressBar.setIndeterminate(false);
							progressBar.setValue(value);
							break;
						}
					}
				});

	}

	@Action
	public void showAboutBox() {
		if (aboutBox == null) {
			JFrame mainFrame = JChessApp.getApplication().getMainFrame();
			aboutBox = new JChessAboutBox(mainFrame);
			aboutBox.setLocationRelativeTo(mainFrame);
		}
		JChessApp.getApplication().show(aboutBox);
	}

	public String showPawnPromotionBox(String color) {
		if (promotionBox == null) {
			JFrame mainFrame = JChessApp.getApplication().getMainFrame();
			promotionBox = new PawnPromotionWindow(mainFrame, color);
			promotionBox.setLocationRelativeTo(mainFrame);
			promotionBox.setModal(true);

		}
		promotionBox.setColor(color);
		JChessApp.getApplication().show(promotionBox);

		return promotionBox.result;
	}

	public String showSaveWindow() {

		return "";
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed"
	// desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		mainPanel = new javax.swing.JPanel();
		gamesPane = new jchess.display.windows.JChessTabbedPane();
		menuBar = new javax.swing.JMenuBar();
		javax.swing.JMenu fileMenu = new javax.swing.JMenu();
		newGameItem = new javax.swing.JMenuItem();

		// loadGameItem = new javax.swing.JMenuItem();
		// saveGameItem = new javax.swing.JMenuItem();
		javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
		gameMenu = new javax.swing.JMenu();
		listPieces = new javax.swing.JMenuItem();
		moveBackItem = new javax.swing.JMenuItem();
		moveForwardItem = new javax.swing.JMenuItem();
		rewindToBegin = new javax.swing.JMenuItem();
		rewindToEnd = new javax.swing.JMenuItem();
		// optionsMenu = new javax.swing.JMenu();
		// themeSettingsMenu = new javax.swing.JMenuItem();
		// javax.swing.JMenu helpMenu = new javax.swing.JMenu();
		javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
		statusPanel = new javax.swing.JPanel();
		javax.swing.JSeparator statusPanelSeparator = new javax.swing.JSeparator();
		statusMessageLabel = new javax.swing.JLabel();
		statusAnimationLabel = new javax.swing.JLabel();
		progressBar = new javax.swing.JProgressBar();

		mainPanel.setMaximumSize(new java.awt.Dimension(800, 600));
		mainPanel.setMinimumSize(new java.awt.Dimension(800, 600));
		mainPanel.setName("mainPanel"); // NOI18N
		mainPanel.setPreferredSize(new java.awt.Dimension(800, 600));

		gamesPane.setName("gamesPane"); // NOI18N

		javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(
				mainPanel);
		mainPanel.setLayout(mainPanelLayout);
		mainPanelLayout.setHorizontalGroup(mainPanelLayout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				mainPanelLayout
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(gamesPane,
								javax.swing.GroupLayout.DEFAULT_SIZE, 776,
								Short.MAX_VALUE).addContainerGap()));
		mainPanelLayout.setVerticalGroup(mainPanelLayout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				mainPanelLayout
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(gamesPane,
								javax.swing.GroupLayout.DEFAULT_SIZE, 580,
								Short.MAX_VALUE)));

		menuBar.setName("menuBar"); // NOI18N

		org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application
				.getInstance(jchess.JChessApp.class).getContext()
				.getResourceMap(JChessView.class);
		fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
		fileMenu.setName("fileMenu"); // NOI18N

		newGameItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
				java.awt.event.KeyEvent.VK_N,
				java.awt.event.InputEvent.CTRL_MASK));
		newGameItem.setText(resourceMap.getString("newGameItem.text")); // NOI18N
		newGameItem.setName("newGameItem"); // NOI18N
		fileMenu.add(newGameItem);
		newGameItem.addActionListener(this);

		// Set time for current game part
		setTimeMenu = new JMenu("Set game time");
		setTime1 = new JMenuItem("1");
		setTime3 = new JMenuItem("3");
		setTime5 = new JMenuItem("5");
		setTime8 = new JMenuItem("8");
		setTime10 = new JMenuItem("10");
		setTime15 = new JMenuItem("15");
		setTime20 = new JMenuItem("20");
		setTime25 = new JMenuItem("25");
		setTime30 = new JMenuItem("30");
		setTime60 = new JMenuItem("60");
		setTime120 = new JMenuItem("120");

		setTime1.addActionListener(this);
		setTime3.addActionListener(this);
		setTime5.addActionListener(this);
		setTime8.addActionListener(this);
		setTime10.addActionListener(this);
		setTime15.addActionListener(this);
		setTime20.addActionListener(this);
		setTime25.addActionListener(this);
		setTime30.addActionListener(this);
		setTime60.addActionListener(this);
		setTime120.addActionListener(this);

		setTimeMenu.add(setTime1);
		setTimeMenu.add(setTime3);
		setTimeMenu.add(setTime5);
		setTimeMenu.add(setTime8);
		setTimeMenu.add(setTime10);
		setTimeMenu.add(setTime15);
		setTimeMenu.add(setTime20);
		setTimeMenu.add(setTime25);
		setTimeMenu.add(setTime30);
		setTimeMenu.add(setTime60);
		setTimeMenu.add(setTime120);
		// setTimeMenu.setEnabled(false);
		fileMenu.add(setTimeMenu);
		/*
		 * loadGameItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt
		 * .event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
		 * loadGameItem.setText(resourceMap.getString("loadGameItem.text")); //
		 * NOI18N loadGameItem.setName("loadGameItem"); // NOI18N
		 * fileMenu.add(loadGameItem); loadGameItem.addActionListener(this);
		 */

		/*
		 * saveGameItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt
		 * .event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
		 * saveGameItem.setText(resourceMap.getString("saveGameItem.text")); //
		 * NOI18N saveGameItem.setName("saveGameItem"); // NOI18N
		 * fileMenu.add(saveGameItem); saveGameItem.addActionListener(this);
		 */

		javax.swing.ActionMap actionMap = org.jdesktop.application.Application
				.getInstance(jchess.JChessApp.class).getContext()
				.getActionMap(JChessView.class, this);
		exitMenuItem.setAction(actionMap.get("quit")); // NOI18N
		exitMenuItem.setName("exitMenuItem"); // NOI18N
		fileMenu.add(exitMenuItem);

		menuBar.add(fileMenu);

		gameMenu.setText(resourceMap.getString("gameMenu.text")); // NOI18N
		gameMenu.setName("gameMenu"); // NOI18N

		listPieces.setName("listPieces");
		listPieces.setText("List pieces"); // FIXME use resourcemap
		listPieces.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				displayPiecesList();
			}
		});
		gameMenu.add(listPieces);
		moveBackItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
				java.awt.event.KeyEvent.VK_Z,
				java.awt.event.InputEvent.CTRL_MASK));
		moveBackItem.setText(resourceMap.getString("moveBackItem.text")); // NOI18N
		moveBackItem.setName("moveBackItem"); // NOI18N
		moveBackItem.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				moveBackItemMouseClicked(evt);
			}
		});
		moveBackItem.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				moveBackItemActionPerformed(evt);
			}
		});
		gameMenu.add(moveBackItem);

		moveForwardItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
				java.awt.event.KeyEvent.VK_Y,
				java.awt.event.InputEvent.CTRL_MASK));
		moveForwardItem.setText(resourceMap.getString("moveForwardItem.text")); // NOI18N
		moveForwardItem.setName("moveForwardItem"); // NOI18N
		moveForwardItem.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				moveForwardItemMouseClicked(evt);
			}
		});
		moveForwardItem.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				moveForwardItemActionPerformed(evt);
			}
		});
		gameMenu.add(moveForwardItem);

		rewindToBegin.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
				java.awt.event.KeyEvent.VK_Z,
				java.awt.event.InputEvent.SHIFT_MASK
						| java.awt.event.InputEvent.CTRL_MASK));
		rewindToBegin.setText(resourceMap.getString("rewindToBegin.text")); // NOI18N
		rewindToBegin.setName("rewindToBegin"); // NOI18N
		rewindToBegin.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rewindToBeginActionPerformed(evt);
			}
		});
		gameMenu.add(rewindToBegin);

		rewindToEnd.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
				java.awt.event.KeyEvent.VK_Y,
				java.awt.event.InputEvent.SHIFT_MASK
						| java.awt.event.InputEvent.CTRL_MASK));
		rewindToEnd.setText(resourceMap.getString("rewindToEnd.text")); // NOI18N
		rewindToEnd.setName("rewindToEnd"); // NOI18N
		rewindToEnd.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rewindToEndActionPerformed(evt);
			}
		});
		gameMenu.add(rewindToEnd);

		menuBar.add(gameMenu);

		/*
		 * optionsMenu.setText(resourceMap.getString("optionsMenu.text")); //
		 * NOI18N optionsMenu.setName("optionsMenu"); // NOI18N
		 * 
		 * themeSettingsMenu.setText(resourceMap.getString("themeSettingsMenu.text"
		 * )); // NOI18N themeSettingsMenu.setName("themeSettingsMenu"); //
		 * NOI18N optionsMenu.add(themeSettingsMenu);
		 * themeSettingsMenu.addActionListener(this);
		 * 
		 * menuBar.add(optionsMenu);
		 */

		/*
		 * helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
		 * helpMenu.setName("helpMenu"); // NOI18N
		 * 
		 * aboutMenuItem.setAction(actionMap.get("showAboutBox")); // NOI18N
		 * aboutMenuItem.setName("aboutMenuItem"); // NOI18N
		 * helpMenu.add(aboutMenuItem);
		 * 
		 * menuBar.add(helpMenu);
		 */

		statusPanel.setName("statusPanel"); // NOI18N

		statusPanelSeparator.setName("statusPanelSeparator"); // NOI18N

		statusMessageLabel.setName("statusMessageLabel"); // NOI18N

		statusAnimationLabel
				.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		statusAnimationLabel.setName("statusAnimationLabel"); // NOI18N

		progressBar.setName("progressBar"); // NOI18N

		javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(
				statusPanel);
		statusPanel.setLayout(statusPanelLayout);
		statusPanelLayout
				.setHorizontalGroup(statusPanelLayout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addComponent(statusPanelSeparator,
								javax.swing.GroupLayout.DEFAULT_SIZE, 800,
								Short.MAX_VALUE)
						.addGroup(
								statusPanelLayout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(statusMessageLabel)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED,
												616, Short.MAX_VALUE)
										.addComponent(
												progressBar,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(statusAnimationLabel)
										.addContainerGap()));
		statusPanelLayout
				.setVerticalGroup(statusPanelLayout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								statusPanelLayout
										.createSequentialGroup()
										.addComponent(
												statusPanelSeparator,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												2,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addGroup(
												statusPanelLayout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(
																statusMessageLabel)
														.addComponent(
																statusAnimationLabel)
														.addComponent(
																progressBar,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(3, 3, 3)));

		setComponent(mainPanel);
		setMenuBar(menuBar);
		setStatusBar(statusPanel);
	}// </editor-fold>//GEN-END:initComponents

	private void moveBackItemActionPerformed(java.awt.event.ActionEvent evt)// GEN-FIRST:event_moveBackItemActionPerformed
	{// GEN-HEADEREND:event_moveBackItemActionPerformed
		if (getGui() != null && getGui().getGame() != null) {
			getGui().getGame().undo();
		} else {
			try {
				Game activeGame = this.getActiveTabGame();
				if (!activeGame.undo()) {
					JOptionPane.showMessageDialog(null,
							Settings.lang("noMoreUndoMovesInMemory"));
				}
			} catch (java.lang.ArrayIndexOutOfBoundsException exc) {
				JOptionPane.showMessageDialog(null,
						Settings.lang("activeTabDoesNotExists"));
			} catch (UnsupportedOperationException exc) {
				JOptionPane.showMessageDialog(null, exc.getMessage());
			}
		}

	}// GEN-LAST:event_moveBackItemActionPerformed

	private void moveBackItemMouseClicked(java.awt.event.MouseEvent evt)// GEN-FIRST:event_moveBackItemMouseClicked
	{// GEN-HEADEREND:event_moveBackItemMouseClicked
		// TODO add your handling code here:

	}// GEN-LAST:event_moveBackItemMouseClicked

	private void moveForwardItemMouseClicked(java.awt.event.MouseEvent evt)// GEN-FIRST:event_moveForwardItemMouseClicked
	{// GEN-HEADEREND:event_moveForwardItemMouseClicked
		// TODO add your handling code here:

	}// GEN-LAST:event_moveForwardItemMouseClicked

	private void moveForwardItemActionPerformed(java.awt.event.ActionEvent evt)// GEN-FIRST:event_moveForwardItemActionPerformed
	{// GEN-HEADEREND:event_moveForwardItemActionPerformed
		// TODO add your handling code here:
		if (getGui() != null && getGui().getGame() != null) {
			getGui().getGame().redo();
		} else {
			try {
				Game activeGame = this.getActiveTabGame();
				if (!activeGame.redo()) {
					JOptionPane.showMessageDialog(null,
							Settings.lang("noMoreRedoMovesInMemory"));
				}
			} catch (java.lang.ArrayIndexOutOfBoundsException exc) {
				JOptionPane.showMessageDialog(null,
						Settings.lang("activeTabDoesNotExists"));
			} catch (UnsupportedOperationException exc) {
				JOptionPane.showMessageDialog(null, exc.getMessage());
			}
		}
	}// GEN-LAST:event_moveForwardItemActionPerformed

	private void rewindToBeginActionPerformed(java.awt.event.ActionEvent evt)// GEN-FIRST:event_rewindToBeginActionPerformed
	{// GEN-HEADEREND:event_rewindToBeginActionPerformed
		try {
			Game activeGame = this.getActiveTabGame();
			if (!activeGame.rewindToBegin()) {
				JOptionPane.showMessageDialog(null,
						Settings.lang("noMoreRedoMovesInMemory"));
			}
		} catch (ArrayIndexOutOfBoundsException exc) {
			JOptionPane.showMessageDialog(null,
					Settings.lang("activeTabDoesNotExists"));
		} catch (UnsupportedOperationException exc) {
			JOptionPane.showMessageDialog(null, exc.getMessage());
		}
	}// GEN-LAST:event_rewindToBeginActionPerformed

	private void rewindToEndActionPerformed(java.awt.event.ActionEvent evt)// GEN-FIRST:event_rewindToEndActionPerformed
	{// GEN-HEADEREND:event_rewindToEndActionPerformed
		try {
			Game activeGame = this.getActiveTabGame();
			if (!activeGame.rewindToEnd()) {
				JOptionPane.showMessageDialog(null,
						Settings.lang("noMoreUndoMovesInMemory"));
			}
		} catch (ArrayIndexOutOfBoundsException exc) {
			JOptionPane.showMessageDialog(null,
					Settings.lang("activeTabDoesNotExists"));
		} catch (UnsupportedOperationException exc) {
			JOptionPane.showMessageDialog(null, exc.getMessage());
		}
	}// GEN-LAST:event_rewindToEndActionPerformed

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private JMenu gameMenu;
	private JTabbedPane gamesPane;
	// private JMenuItem loadGameItem;
	public JPanel mainPanel;
	private JMenuBar menuBar;
	private JMenuItem moveBackItem;
	private JMenuItem moveForwardItem;
	private JMenu setTimeMenu;
	private JMenuItem setTime1;
	private JMenuItem setTime3;
	private JMenuItem setTime5;
	private JMenuItem setTime8;
	private JMenuItem setTime10;
	private JMenuItem setTime15;
	private JMenuItem setTime20;
	private JMenuItem setTime25;
	private JMenuItem setTime30;
	private JMenuItem setTime60;
	private JMenuItem setTime120;
	private JMenuItem newGameItem;
	// private JMenu optionsMenu;
	private JProgressBar progressBar;
	private JMenuItem listPieces;
	private JMenuItem rewindToBegin;
	private JMenuItem rewindToEnd;
	// private JMenuItem saveGameItem;
	private JLabel statusAnimationLabel;
	private JLabel statusMessageLabel;
	private JPanel statusPanel;
	private JMenuItem themeSettingsMenu;
	// End of variables declaration//GEN-END:variables
	// private JTabbedPaneWithIcon gamesPane;
	private final Timer messageTimer;
	private final Timer busyIconTimer;
	private final Icon idleIcon;
	private final Icon[] busyIcons = new Icon[15];
	private int busyIconIndex = 0;

	private JDialog aboutBox;
	private PawnPromotionWindow promotionBox;
	private JDialog newGameFrame;

	@Override
	public void componentResized(ComponentEvent e) {
		LOG.debug("jchessView has been resized !");
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public Game getActiveTabGame() throws ArrayIndexOutOfBoundsException {
		Game activeGame = (Game) this.gamesPane.getComponentAt(this.gamesPane
				.getSelectedIndex());
		return activeGame;
	}

	public void disableSetTimeMenu() {
		this.setTimeMenu.setEnabled(false);
	}

	public void setActiveTabGame(int index)
			throws ArrayIndexOutOfBoundsException {
		this.gamesPane.setSelectedIndex(index);
	}

	public void setLastTabAsActive() {
		this.gamesPane.setSelectedIndex(this.gamesPane.getTabCount() - 1);
	}

	public int getNumberOfOpenedTabs() {
		return this.gamesPane.getTabCount();
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void componentShown(ComponentEvent e) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void componentHidden(ComponentEvent e) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public void displayPiecesList() {
		Chessboard board = this.getActiveTabGame().getChessboard();
		String out = "";
		int[] wFigures = new int[6]; //0=pawn, 1=rook, 2=bishop, 3=knight, 4=queen, 5=king
		int[] bFigures = new int[6];
		int wScore = 0;
		int bScore = 0;
		for (Piece p : board.getAllPieces(Colors.WHITE)) {
			if (p instanceof Pawn)
				wFigures[0]++;
			else if (p instanceof Rook)
				wFigures[1]++;
			else if (p instanceof Bishop)
				wFigures[2]++;
			else if (p instanceof Knight)
				wFigures[3]++;
			else if (p instanceof Queen)
				wFigures[4]++;
			else if (p instanceof King)
				wFigures[5]++;
			wScore += p.getScore();
		}

		for (Piece p : board.getAllPieces(Colors.BLACK)) {
			if (p instanceof Pawn)
				bFigures[0]++;
			else if (p instanceof Rook)
				bFigures[1]++;
			else if (p instanceof Bishop)
				bFigures[2]++;
			else if (p instanceof Knight)
				bFigures[3]++;
			else if (p instanceof Queen)
				bFigures[4]++;
			else if (p instanceof King)
				bFigures[5]++;
			bScore += p.getScore();
		}

		out += "White player\n";
		for (int i=0; i<wFigures.length; i++)
			if (i == 0)
				out += "Pawn: " + wFigures[0]++ + "\n";
			else if (i == 1)
				out += "Bishop: " + wFigures[1]++ + "\n";
			else if (i == 2)
				out += "Knight: " + wFigures[2]++ + "\n";
			else if (i == 3)
				out += "Queen: " + wFigures[3]++ + "\n";
			else if (i == 4)
				out += "King: " + wFigures[4]++ + "\n";
			else if (i == 5)
				wFigures[5]++;
		out += "White score: " + wScore + "\n\n";
		
		out += "Black player\n";
		for (int i=0; i<bFigures.length; i++)
			if (i == 0)
				out += "Pawn: " + bFigures[0]++ + "\n";
			else if (i == 1)
				out += "Bishop: " + bFigures[1]++ + "\n";
			else if (i == 2)
				out += "Knight: " + bFigures[2]++ + "\n";
			else if (i == 3)
				out += "Queen: " + bFigures[3]++ + "\n";
			else if (i == 4)
				out += "King: " + bFigures[4]++ + "\n";
			else if (i == 5)
				bFigures[5]++;
		out += "Black score: " + bScore;
		System.out.println(out);
	}

	/**
	 * @return the newGameFrame
	 */
	public JDialog getNewGameFrame() {
		return newGameFrame;
	}

	/**
	 * @param newGameFrame
	 *            the newGameFrame to set
	 */
	public void setNewGameFrame(JDialog newGameFrame) {
		this.newGameFrame = newGameFrame;
	}

}
