package jchess.core.pieces.implementation;

import jchess.core.pieces.Piece;
import jchess.core.pieces.traits.behaviors.implementation.LionBehavior;
import jchess.core.Chessboard;
import jchess.core.Player;

public class Lion extends Piece
{

  protected static final short value = 6;

  public Lion(Chessboard chessboard, Player player)
  {
    super(chessboard, player);//call initializer of super type: Piece
    this.symbol = "L";
    this.score = 4;
    this.addBehavior(new LionBehavior(this));
  }
}
