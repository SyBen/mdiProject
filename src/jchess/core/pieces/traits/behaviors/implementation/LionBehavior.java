package jchess.core.pieces.traits.behaviors.implementation;

import java.util.HashSet;
import java.util.Set;

import jchess.core.Square;
import jchess.core.pieces.Piece;
import jchess.core.pieces.traits.behaviors.Behavior;

public class LionBehavior extends Behavior
{

  public LionBehavior(Piece piece) {
    super(piece);
  }

  @Override
  public Set<Square> getSquaresInRange() {
    Set<Square> list   = new HashSet<>();
    Square[][] squares = piece.getChessboard().getSquares();

    int pozX = piece.getSquare().getPozX();
    int pozY = piece.getSquare().getPozY();

    int[][] squaresInRange = {
      {pozX - 2, pozY + 1}, //1
      {pozX - 1, pozY + 2}, //2
      {pozX + 1, pozY + 2}, //3
      {pozX + 2, pozY + 1}, //4
      {pozX + 2, pozY - 1}, //5
      {pozX + 1, pozY - 2}, //6
      {pozX - 1, pozY - 2}, //7
      {pozX - 2, pozY - 1}, //8
    };

    for(int[] squareCoordinates : squaresInRange)
    {
      int x = squareCoordinates[0];
      int y = squareCoordinates[1];
      if (!piece.isOut(x, y))
      {
        list.add(squares[x][y]);
      }
    }
    return list;
  }

}
