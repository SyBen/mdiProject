package jchess.core;

import java.awt.Point;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

public class RandomPlacementMethod implements PlacementMethod {

  private boolean hasGenerated = false;

  private List<Point> whitePawns;
  private List<Point> whiteRooks;
  private List<Point> whiteKnights;
  private List<Point> whiteBishops;
  private List<Point> whiteLions;
  private Point       whiteKing;
  private Point       whiteQueen;

  private List<Point> blackPawns;
  private List<Point> blackRooks;
  private List<Point> blackKnights;
  private List<Point> blackBishops;
  private List<Point> blackLions;
  private Point       blackKing;
  private Point       blackQueen;

  public RandomPlacementMethod() {
    whitePawns   = new ArrayList<Point>();
    whiteRooks   = new ArrayList<Point>();
    whiteKnights = new ArrayList<Point>();
    whiteBishops = new ArrayList<Point>();
    whiteLions   = new ArrayList<Point>();
    whiteKing    = null;
    whiteQueen   = null;

    blackPawns   = new ArrayList<Point>();
    blackRooks   = new ArrayList<Point>();
    blackKnights = new ArrayList<Point>();
    blackBishops = new ArrayList<Point>();
    blackLions   = new ArrayList<Point>();
    blackKing    = null;
    blackQueen   = null;

  }

  @Override
  public void generate(int n, int m) {
    //FIXME use linked list or deque
    ArrayList<Point> blackPoints = new ArrayList<Point>();
    ArrayList<Point> whitePoints = new ArrayList<Point>();

    for (int i=0; i<2; i++)
      for (int j=0; j<n; j++)
        blackPoints.add(new Point(j, i));

    for (int i=(m-2); i<m; i++)
      for (int j=0; j<n; j++)
        whitePoints.add(new Point(j, i));

    Collections.shuffle(blackPoints);
    Collections.shuffle(whitePoints);

    for (int i=0; i<8; i++)
      whitePawns.add(whitePoints.remove(0));
    whiteRooks.add(whitePoints.remove(0));
    whiteRooks.add(whitePoints.remove(0));
    whiteBishops.add(whitePoints.remove(0));
    whiteBishops.add(whitePoints.remove(0));
    whiteKnights.add(whitePoints.remove(0));
    whiteKnights.add(whitePoints.remove(0));
    whiteKing  = whitePoints.remove(0);
    whiteQueen = whitePoints.remove(0);

    for (int i=0; i<8; i++)
      blackPawns.add(blackPoints.remove(0));
    blackRooks.add(blackPoints.remove(0));
    blackRooks.add(blackPoints.remove(0));
    blackBishops.add(blackPoints.remove(0));
    blackBishops.add(blackPoints.remove(0));
    blackKnights.add(blackPoints.remove(0));
    blackKnights.add(blackPoints.remove(0));
    blackKing  = blackPoints.remove(0);
    blackQueen = blackPoints.remove(0);

    //whiteRooks.add(new Point(0, 7));
    //whiteRooks.add(new Point(7, 7));
    //whiteBishops.add(new Point(1, 7));
    //whiteBishops.add(new Point(6, 7));
    //whiteKnights.add(new Point(2, 7));
    //whiteKnights.add(new Point(5, 7));
    //whiteKing  = new Point(4, 7);
    //whiteQueen = new Point(3, 7);

    //blackRooks.add(new Point(0, 0));
    //blackRooks.add(new Point(7, 0));
    //blackBishops.add(new Point(1, 0));
    //blackBishops.add(new Point(6, 0));
    //blackKnights.add(new Point(2, 0));
    //blackKnights.add(new Point(5, 0));
    //blackKing  = new Point(3, 0);
    //blackQueen = new Point(4, 0);
  }

  @Override
  public List<Point> getWhitePawns() {
    return this.whitePawns;
  }

  @Override
  public List<Point> getWhiteRooks() {
    return this.whiteRooks;
  }

  @Override
  public List<Point> getWhiteKnights() {
    return this.whiteKnights;
  }

  @Override
  public List<Point> getWhiteBishops() {
    return this.whiteBishops;
  }

  @Override
  public List<Point> getWhiteLions() {
    return this.whiteLions;
  }

  @Override
  public Point getWhiteKing() {
    return this.whiteKing;
  }

  @Override
  public Point getWhiteQueen() {
    return this.whiteQueen;
  }

  @Override
  public List<Point> getBlackPawns() {
    return blackPawns;
  }

  @Override
  public List<Point> getBlackRooks() {
    return blackRooks;
  }

  @Override
  public List<Point> getBlackKnights() {
    return blackKnights;
  }

  @Override
  public List<Point> getBlackBishops() {
    return this.blackBishops;
  }

  @Override
  public List<Point> getBlackLions() {
    return this.blackLions;
  }

  @Override
  public Point getBlackKing() {
    return blackKing;
  }

  @Override
  public Point getBlackQueen() {
    return blackQueen;
  }


}
