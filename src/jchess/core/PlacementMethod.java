package jchess.core;

import java.util.List;

import java.awt.Point;

/**
 * Interface to define method for chess pieces placement.
 */
public interface PlacementMethod {

  /** 
   * Initialize position for a new chessboard
   * @param n number of line of the board
   * @param m number of column of the board
   */
  public void generate(int n, int m);

  public List<Point> getWhitePawns();
  public List<Point> getWhiteRooks();
  public List<Point> getWhiteKnights();
  public List<Point> getWhiteBishops();
  public List<Point> getWhiteLions();
  public Point getWhiteKing();
  public Point getWhiteQueen();

  public List<Point> getBlackPawns();
  public List<Point> getBlackRooks();
  public List<Point> getBlackKnights();
  public List<Point> getBlackBishops();
  public List<Point> getBlackLions();
  public Point getBlackKing();
  public Point getBlackQueen();
}
