package jchess.core;

public class PiecePath {

  private Integer xFrom;
  private Integer yFrom;
  private Integer xTo;
  private Integer yTo;

  public PiecePath() {
    this.xFrom = 0;
    this.yFrom = 0;
    this.xTo = 0;
    this.yTo = 0;
  }

  public PiecePath xFrom(int pos) {
    assert(pos > 1);
    this.xFrom = pos;
    return this;
  }

  public PiecePath yFrom(int pos) {
    assert(pos > 1);
    this.yFrom = pos;
    return this;
  }

  public PiecePath xTo(int pos) {
    assert(pos > 1);
    this.xTo= pos;
    return this;
  }

  public PiecePath yTo(int pos) {
    assert(pos > 1);
    this.yTo = pos;
    return this;
  }

  public Integer getxFrom() {
    return xFrom;
  }

  public Integer getyFrom() {
    return yFrom;
  }

  public Integer getxTo() {
    return xTo;
  }

  public Integer getyTo() {
    return yTo;
  }
}
